from django.shortcuts import render,get_object_or_404,reverse
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from myapp.models import Contact_us,category,register_table,add_property,cart,order
from django.contrib.auth.models import User
from django.contrib.auth import login,authenticate,logout
import requests
from django.contrib.auth.decorators import login_required
from myapp.forms import add_product_form
from django.db.models import Q
from datetime import datetime
from django.core.mail import EmailMessage
import random

def first(request):
    if "user_id" in request.COOKIES:
        uid = request.COOKIES["user_id"]
        usr = get_object_or_404(User,id=uid)
        login (request,usr)
        if usr.is_superuser:
                return HttpResponseRedirect("/admin")
        if usr.is_staff:
                return HttpResponseRedirect("/seller_dashboard")
        if usr.is_active:
                return HttpResponseRedirect("/cus_dasboard")  
    recent = Contact_us.objects.all().order_by("-id")[:5]
    cats = category.objects.all()
    # print(cats)
    return render(request,"first.html",{"message":recent,"catogeries":cats})

def home(r):
    return HttpResponse("<h1 style='color:yellowgreen'>Welcome to HOME PAGE</h1>")

def index(request):
    str = "I AM FROM VIEWS FILE"
    data =["red","green","yellow","blue","black"]
    context ={"colors":data,"string":str}
    return render(request,"index.html",context)

def country(request):
    data=requests.get("https://restcountries.eu/rest/v2/all").json()   
    #  json [:10] for slicing the data it means to show only 10 countries's data
    return render(request,"countrydata.html",{"con":data,"total":len(data)})    

def contact(request):
    all_data = Contact_us.objects.all().order_by("-id")
    # print(data)
    if request.method=="POST" :
        name = request.POST["name"]
        email = request.POST["email"]
        con = request.POST["phone"]
        message = request.POST["message"]
        data = Contact_us(name= name,Your_Email= email,Phone_No= con,Message= message)
        data.save()
        res = "Dear {} Thanks for your feedback".format(name)
        return render(request,"contact.html",{"status":res,"message":all_data})  
        # return HttpResponse("<h1 style='color:green;'>Dear {} Welcome to our site!!</h1>".format(name))
        
       

    return render(request,"contact.html",{"message":all_data})    

def base(request):
    return render(request,"base.html")    

def register(request):
    if request.method=="POST":
        ut= request.POST["inlineRadioOptions"]
        fname= request.POST["firstname"]
        lname=request.POST["lastname"]
        uname=request.POST["username"]
        email=request.POST["email"]
        pwd=request.POST["password"]
        con=request.POST["phone"]

        usr =User.objects.create_user(uname,email,pwd)
        usr.first_name = fname
        usr.last_name = lname
        if ut=="option1":
            usr.is_staff=True
        if ut=="option2":
            usr.is_active=True
        if ut=="option3":
            usr.is_active=True        
        usr.save()

        reg=register_table(user=usr,contact_number=con)
        reg.save()
        return render(request,"registeration.html",{"status":"{} Account Created Successfully!!".format(fname)})
        # print(request.POST)
    return render(request,"registeration.html")

def check_user(request):
    if request.method=="GET":
        un = request.GET["usern"]
        check = User.objects.filter(username=un)
        # above query check if username entered in username field is already existed in user model(built in model)
        # print(check,len(check))
        # return HttpResponse(check)
        if len(check)== 1:
            return  HttpResponse("existed")
        else:
            return HttpResponse("Not Existed")

def user_login(request):
    if request.method=="POST":
        un=request.POST["username"]
        pwd=request.POST["password"]
        
        user = authenticate(username=un,password=pwd)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("admin/")
            if user.is_staff:
                res = HttpResponseRedirect("/seller_dashboard")
                if "rememberme" in request.POST:
                    res.set_cookie("user_id",user.id)
                    res.set_cookie("data_login",datetime.now())
                return res
            if user.is_active:
                return HttpResponseRedirect("/cus_dasboard")  
        else:
            return render(request,"first.html",{"status":"Invalid Username or Password"})
        
    
    return HttpResponse("Called")
@login_required
def cust_dashboard(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    return render(request,"cus_dasboard.html",context) 
@login_required
def seller_dashboard(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data= register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    return render(request,"seller_dashboard.html",context)
@login_required
def user_logout(request):
    logout(request)  
    res = HttpResponseRedirect("/")
    res.delete_cookie("user_id")
    res.delete_cookie("date_login")
    return res 

def edit_profile(request):
    context={}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data= register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    if request.method=="POST":
        # print(request.POST)
        print(request.FILES)
        fn=request.POST["fname"]
        ln=request.POST["lname"]
        em=request.POST["email"]
        city=request.POST["city"]
        age=request.POST["age"]
        con=request.POST["phone"]
        gender=request.POST["gender"]
        occ=request.POST["occupation"]
        # abt=request.POST["about"]


        usr= User.objects.get(id=request.user.id)
        usr.first_name=fn
        usr.last_name=ln
        usr.email=em
        usr.save()

        data.contact_number=con
        data.age=age
        data.gender=gender
        data.city=city
        data.occupation=occ
        # data.about=abt
        data.save()
        if "profile" in request.FILES:
            img=request.FILES["profile"]
            data.profile_pic = img
            data.save()
        context["status"]="Changes Saved Successfully"

    return render(request,"edit_profile.html",context)

def change_password(request):
    context={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
         data= register_table.objects.get(user__id=request.user.id)
         context["data"]=data
    if request.method == "POST":
        current_pwd = request.POST["currentpass"]
        n_pwd = request.POST["newpass"]
        # print(current_pwd,n_pwd)
        user = User.objects.get(id=request.user.id)
        un=user.username
        check = user.check_password(current_pwd)
        # print(check)

        if check==True:
            user.set_password(n_pwd)
            user.save()
            context["msg"]="Change Password Successfully!!"
            context["col"]="alert-success"
            user=User.objects.get(username=un)
            login(request,user)
        else:
            context["msg"]="Incorrect Current Password"
            context["col"]="alert-danger"
    return render(request,"changePassword.html",context)

def add_product_view(request):
    context={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
         data= register_table.objects.get(user__id=request.user.id)
         context["data"]=data
    form = add_product_form()
    if request.method=="POST":
        form = add_product_form(request.POST,request.FILES)
        if form.is_valid():
            data= form.save(commit=False)
            login_user =User.objects.get(username=request.user.username)
            data.seller = login_user
            data.save()
            context["status"]="{} Added Successfully".format(data.property_name)

    context["form"]= form 
    return render(request,"addproduct.html",context)

def my_properties(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
         data= register_table.objects.get(user__id=request.user.id)
         context["data"]=data
    
    all = add_property.objects.filter(seller__id=request.user.id)
    context["properties"]=all
    return render(request,"myproperties.html",context)

def view_property(request):
    context ={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data= register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    id= request.GET["pid"]
    obj = add_property.objects.get(id=id)
    context["property"] = obj
    return render(request,"view_property.html",context)

def update_property(request):
    context ={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data= register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    cats = category.objects.all()
    context["category"] = cats
    pid = request.GET["pid"]
    property = get_object_or_404(add_property,id=pid)
    property = add_property.objects.get(id=pid)
    context["property"] = property

    if request.method == "POST":
        pn= request.POST["pname"]
        cat_id = request.POST["pcat"]
        pp = request.POST["price"]
        sp = request.POST["sprice"]
        des= request.POST["ds"]
        cat_obj = category.objects.get(id=cat_id)

        property.property_name = pn
        property.property_category = cat_obj
        property.property_price = pp
        property.Sale_price = sp
        property.Details = des

        if "pimg" in request.FILES:
            img = request.FILES["pimg"]
            property.property_name = img 
        property.save()
        context["status"]= "Changes Saved successfully"
        context["id"] = pid    

    return render(request,"update_property.html",context)

def delete_property(request):
    context={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data= register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    if "pid" in request.GET:
        pid = request.GET["pid"]
        prop = get_object_or_404(add_property,id=pid)
        context["property"]= prop

        if "action" in request.GET:
            prop.delete()
            context["status"]= str(prop.property_name)+"Removed Successfully!!!"
    return render(request,"deleteproperty.html",context)

def all_property(request):
    context={}
    all = add_property.objects.all().order_by("property_name")
    context["property"] = all
    if "qry"in request.GET:
        q = request.GET["qry"]
        prop = add_property.objects.filter(Q(property_name__contains=q)|Q(property_category__cat_name__contains=q))
        context["property"]= prop
        context["abcd"]="search"
    if "cat" in request.GET:
        cid= request.GET["cat"]
        prop = add_property.objects.filter(property_category__id=cid)
        context["property"]= prop
        context["abcd"]="search"  

    return render(request,"allproperties.html",context)

def sendemail(request):
    # print(request)
    context = {}
    if request.method=="POST":
        rec = request.POST["to"].split(",")
        print(rec)
        sub = request.POST["sub"]
        msg = request.POST["msg"]
        try:
            em = EmailMessage(sub,msg,to=rec)
            em.send()
            context["message"] = "Email Sent"
            context["cls"] = "alert-success"
        except:
            context["message"] = "Check Your Internet Connection/Email Address"
            context["cls"] = "alert-danger"

    return render(request,"sendemail.html",context)

def forgotpass(request):
    context = {}
    if request.method=="POST":
        
        un = request.POST["username"]
        pwd = request.POST["npass"]
        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()
        context["status"] = "Password Change Successfully"
    return render(request,"forgetpass.html",context)    

def reset_pass(request):
    un =  request.GET["username"]
    try:
        user = get_object_or_404(User,username=un)  
        otp = random.randint(1000,9990)
        msg = "Dear {} \n {} is your One Time Password (OTP)\n Donot share it others\n Thanks&Regards\n EVERNEST ".format(user.username,otp)
        try:
            email = EmailMessage("Account Verification",msg,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})
        except:
            return JsonResponse({"status":"error","email":user.email})
    except:
        return JsonResponse({"status":"failed"})

def add_cart_to(request):
    context = {}
    items = cart.objects.filter(user__id=request.user.id,status=False)
    context["items"]=items
    if request.user.is_authenticated:
        if request.method=="POST":
            pid = request.POST["pid"]
            qty = request.POST["qty"]
            property = get_object_or_404(add_property,id=pid)
            is_exist = cart.objects.filter(product__id=pid,user__id=request.user.id,status=False)
            if len(is_exist)>0:
                context["msg"]="{} Already added in Your Cart".format(property.property_name)
                context["cls"]="alert alert-danger"
            else:
                property = get_object_or_404(add_property,id=pid)
                usr= get_object_or_404(User,id=request.user.id)
                c = cart(user=usr,product = property,quantity=qty)
                c.save()
                context["msg"]="{}  added in Your Cart".format(property.property_name)
                context["cls"]="alert alert-success"
    else:
        context["status"]= "PLEASE LOGIN TO VIEW CART"
    return render(request,"addtocart.html",context)

def get_cart_data(request):
    items = cart.objects.filter(user__id=request.user.id,status = False)
    sale,total,quantity = 0,0,0
    for i in items:
        print(i.product.Sale_price)
        sale += (i.product.Sale_price)
        total+=  i.product.property_price
        quantity += i.quantity
        
    res = {
        "total": total,"offer":sale,"quan":quantity,
        }
    return JsonResponse(res)

def change_quant(request):
    if "quantity" in request.GET:
        cid = request.GET["cid"]
        qty = request.GET["quantity"]
        cart_obj = get_object_or_404(cart,id=cid)
        cart_obj.quantity = qty
        cart_obj.save()    
        return HttpResponse(cart_obj.quantity)
    if "delete_cart" in request.GET:
        id = request.GET["delete_cart"]  
        cart_obj= get_object_or_404(cart,id=id) 
        cart_obj.delete() 
        return HttpResponse(1)

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings

def process_payment(request):
    items = cart.objects.filter(user_id__id =request.user.id,status=False )
    products = ""
    amt=0
    inv = "INV10001-"
    cart_ids = ""
   
    for j in items:
        products = str(j.product.property_name)+"\n"
        p_ids = str(j.product.id)+","
        amt += j.product.Sale_price
        inv += str(j.id)
        cart_ids += str(j.id)+ ","

    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'item_name': products,
        'invoice': inv,
        
        'notify_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format("127.0.0.1:8000",
                                              reverse('payment_cancelled')),
    }
    usr = User.objects.get(username=request.user.username)
    ord = order(cust_id=usr,cart_ids = cart_ids,product_ids=p_ids)
    ord.save()
    ord.invoice_id =str(ord.id)+inv
    ord.save()

    request.session["order_id"] = ord.id

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'form': form})

def payment_done(request):
    if "order_id" in request.session:
        order_id = request.session["order_id"]
        ord_obj = get_object_or_404(order,id=order_id)
        ord_obj.status=True
        ord_obj.save()
        for i in ord_obj.cart_ids.split(",")[:-1]:
            cart_ob = get_object_or_404(cart,id=i)
            cart_ob.status=True
            cart_ob.save()
    return render(request,"payement_success.html")
def payment_cancelled(request):
    return render(request,"payment_failure.html")

def my_order(request):
    context ={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data= register_table.objects.get(user__id=request.user.id)
        context["data"]=data

    all_orders = []
    orders = order.objects.filter(cust_id__id=request.user.id).order_by("-id")
    
    for o in orders:
        products = []
        for id in o.product_ids.split(",")[:-1]:
            pro = get_object_or_404(add_property,id=id)
            products.append(pro)
            ord={
                "order_id" : o.id,
                "products" : products,
                "invoice" : o.invoice_id,
                "status" : o.status,
                "date" : o.processed_on,
            }
            all_orders.append(ord)
    context["order_history"] = all_orders
    return render(request,"order_history.html",context)    

    

