from django import forms
from myapp.models import  add_property

class add_product_form(forms.ModelForm):
    class Meta:
        model = add_property
        fields = ["property_name","property_category","property_price","Sale_price","property_image","Details"]

