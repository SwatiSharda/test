from django.db import models
from django.contrib.auth.models import User



class Student(models.Model):
    c=(
        ("M","Male"),("F","Female")
    ) 
    name= models.CharField(max_length=250)
    # email= models.EmailField
    roll_no= models.IntegerField(unique=True)
    fee= models.FloatField()
    gender=models.CharField(max_length=150,choices=c)
    address= models.TextField(blank=True)
    is_registered= models.BooleanField()

    def __str__(self):
        return self.name+" "+str(self.roll_no)
class Contact_us(models.Model):
    name=models.CharField(max_length=250)
    Your_Email=models.EmailField(max_length=250)
    Phone_No  =models.IntegerField(unique=True)
    Message=models.TextField()
    added_on=models.DateTimeField(auto_now_add=True) 

    def __str__(self):
        return (self.name) 

    class Meta:
        verbose_name_plural ="Contact_us"

class apidata(models.Model):
    Country_name=models.CharField(max_length=250)
    Capital= models.CharField(max_length=250)
    Population= models.IntegerField()
    
    def __str__(self):
        return (self.Country_name)

    class Meta:
        verbose_name_plural="Country Data"

class category(models.Model):
    cat_name = models.CharField(max_length=250) 
    cover_pic = models.FileField(upload_to= "categories/%Y/%m/%d")
    description = models.TextField()
    added_on=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.cat_name

    class Meta:
        verbose_name_plural="Catrgories" 

class register_table(models.Model):
   
    user = models.OneToOneField(User,on_delete= models.CASCADE)
    contact_number = models.IntegerField()
    profile_pic= models.ImageField(upload_to="profiles/%Y/%m/%d",null=True)
    age=models.CharField(max_length=250,null=True,blank=True)
    gender=models.CharField(max_length=150,default="Male")
    occupation= models.CharField(max_length=250,null=True,blank=True)
    city=models.CharField(max_length=250,null=True,blank=True)
    # about_us=models.CharField(max_length=250,null=True,blank=True)
    added_on=models.DateTimeField(auto_now_add=True,null=True) 
    updated_on= models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.user.username

class add_property(models.Model):
    seller = models.ForeignKey(User,on_delete= models.CASCADE)   
    property_name = models.CharField(max_length=250)
    property_category = models.ForeignKey(category,on_delete = models.CASCADE)
    property_price = models.FloatField()
    Sale_price = models.FloatField()
    property_image = models.ImageField(upload_to = "properties/%Y/%m/%d")
    Details = models.TextField()
    
    def __str__(self):
        return self.property_name

    class Meta:
        verbose_name_plural="Add Properties" 

class cart(models.Model):
    user =  models.ForeignKey(User,on_delete= models.CASCADE) 
    product = models.ForeignKey(add_property,on_delete=models.CASCADE)         
    quantity = models.IntegerField()
    status = models.BooleanField(default=False)
    added_on=models.DateTimeField(auto_now_add=True,null=True) 
    updated_on= models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.user.username

class order(models.Model):
    cust_id = models.ForeignKey(User,on_delete= models.CASCADE)  
    cart_ids = models.CharField(max_length=250)
    product_ids = models.CharField(max_length=250)  
    invoice_id = models.CharField(max_length=250)
    status = models.BooleanField(default=False)
    processed_on = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.cust_id.username





 
