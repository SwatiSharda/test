from django.contrib import admin
from myapp.models import Student,Contact_us,apidata,category,register_table,add_property,cart,order

admin.site.site_header="My Website"

class StudentAdmin(admin.ModelAdmin):
    # fields=["roll_no", "name","gender"] for displaying particular fields
    list_display = ["roll_no","name","gender","address","fee","is_registered"]
    search_fields =["roll_no","name"]
    list_filter =["roll_no","name","gender"]
    list_editable =["gender","fee","is_registered"]

class register_tableAdmin(admin.ModelAdmin):
    list_display = ["user","age","city","added_on","updated_on"]

class orderAdmin(admin.ModelAdmin):
    list_display = ["cust_id","status","processed_on"]

class cartAdmin(admin.ModelAdmin):
    list_display = ["product","status","added_on","updated_on"]    



class Contact_usAdmin(admin.ModelAdmin): 
    # fields=["Phone_No", "name"]   
    list_display = ["name","Phone_No","Your_Email","Message","added_on"]
    search_fields =["name","Phone_No"]
    list_filter =["added_on"]
    list_editable =["Your_Email","Phone_No"]

class apidataAdmin(admin.ModelAdmin):
    list_display=["Country_name","Capital","Population"]  
    search_fields =["Country_name"]
    list_filter =["Country_name"]


     

admin.site.register(Student,StudentAdmin)
admin.site.register(Contact_us,Contact_usAdmin)
admin.site.register(apidata,apidataAdmin)
admin.site.register(category)
admin.site.register(register_table,register_tableAdmin)
admin.site.register(add_property)
admin.site.register(cart,cartAdmin)
admin.site.register(order,orderAdmin)
